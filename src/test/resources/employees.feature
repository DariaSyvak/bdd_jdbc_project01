Feature: Employees validation

  @DBtest
  Scenario: Validating the database connection
    Given user executes "select first_name,last_name from employees where manager_id=122"
    Then user should see list of the names
      | Jason    | Mallin     |
      | Michael  | Rogers     |
      | Ki       | Gee        |
      | Hazel    | Philtanker |
      | Kelly    | Chung      |
      | Jennifer | Dilly      |
      | Timothy  | Gates      |
      | Randall  | Perkins    |

