package stepDefs;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;
import utils.DBUtil;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class EmployeesValidation {
    private static String result;
    private static List<List<Object>>actual;

    @Given("user executes {string}")
    public void userExecutes(String query) {
        DBUtil.createDBConnection();
        DBUtil.executeQuery(query);
        result = query;

    }

    @Then("user should see list of the names")
    public void userShouldSeeListOfTheName(DataTable dataTable) {
        actual=DBUtil.getQueryResultList(result);
        for (int i = 0; i < dataTable.asLists().size(); i++) {
            Assert.assertEquals(actual.get(i).get(0),dataTable.asLists().get(i).get(0));
            Assert.assertEquals(actual.get(i).get(1),dataTable.asLists().get(i).get(1));

        }
    }
}
